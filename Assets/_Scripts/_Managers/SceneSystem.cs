using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Tabtale.TTPlugins;
public class SceneSystem : MonoBehaviour
{
    [SerializeField] bool hasTutorial = false;
    [SerializeField] bool interstitialLoadingScene = false;
    [SerializeField] bool singleSceneGame = false;

    private float loadingProgress;

    public SceneTypes sceneType;
    public static SceneSystem instance;

    private void Awake()
    {
        TTPCore.Setup();
        if (instance == null)
            instance = this;

        SetEvents(true);
    }

    private void OnDisable()
    {
        SetEvents(false);
    }

    void Start()
    {
        SceneLoader();
    }

    private void SceneLoader()
    {
        switch (sceneType)
        {
            case SceneTypes.None:
                break;
            case SceneTypes.Logo:
                StartCoroutine(LoadLoadingScene());
                break;
            case SceneTypes.Loading:
                StartCoroutine(LoadGameScene());
                break;
            case SceneTypes.Game:
                EventManager.SceneStartEvent();
                break;
            default:
                break;
        }
    }

    IEnumerator LoadLoadingScene()
    {
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(1);
    }

    IEnumerator LoadGameScene()
    {
        yield return new WaitForSeconds(0.5f);
        AsyncOperation loadingOperation = SceneManager.LoadSceneAsync(SceneToLoad());
        while (loadingOperation.progress < 1)
        {
            loadingProgress = loadingOperation.progress;
            yield return new WaitForEndOfFrame();
        }
    }

    private int SceneToLoad()
    {
        int sceneToLoad = 0;
        if (singleSceneGame == false)
        {
            sceneToLoad = ( PreferenceManager.instance.CurrentLevel() - 1) % (SceneManager.sceneCountInBuildSettings - 2) + 2;
        }
        else
            sceneToLoad = 2;

        return sceneToLoad;
    }

    private void LoadLevel()
    {
        if (interstitialLoadingScene)
            StartCoroutine(LoadGameScene());
        else
            SceneManager.LoadScene(SceneToLoad());
    }

    private void SetEvents(bool enable)
    {
        if (enable)
        {
            EventManager.OnSceneEnd += SceneEndSceneSystem;
        }
        else
        {
            EventManager.OnSceneEnd -= SceneEndSceneSystem;
        }
    }

    public void SceneEndSceneSystem()
    {
        LoadLevel();
    }

    public void RestartGame()
    {
        LoadLevel();
    }

   


}
