using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SceneTypes { None, Logo, Loading, Game }

public class ConstantContainer
{
    public class CameraNames
    {
        public const string PLAY_CAM = "PlayCam";
        public const string WIN_CAM = "WinCam";
    }

}
