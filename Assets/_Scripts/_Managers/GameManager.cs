using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class GameManager : MonoBehaviour
{
    public bool hasTutorial;

    public bool didWin;

    public static GameManager instance;


    private void Awake()
    {
        if (instance == null)
            instance = this;
        EventSetter(true);
    }

    private void OnDisable()
    {
        EventSetter(false);
    }

    void Start()
    {
        didWin = false;
    }

    void Update()
    {
        
    }

    private void WinGameManager()
    {
        didWin = true;
    }
    
    private void LoseGameManager()
    {
        didWin = false;
    }

    private void EventSetter(bool enable)
    {
        if (enable)
        {
            EventManager.OnWin += WinGameManager;
            EventManager.OnLose += LoseGameManager;
        }
        else
        {
            EventManager.OnWin -= WinGameManager;
            EventManager.OnLose -= LoseGameManager;
        }
    }

}
