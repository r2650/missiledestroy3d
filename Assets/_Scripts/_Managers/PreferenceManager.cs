using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreferenceManager : MonoBehaviour
{
    public static PreferenceManager instance;

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    public int CurrentLevel()
    {
        return PlayerPrefs.GetInt("CurrentLevel", 1);
    }

    public void SetCurrentLevel(int targetValue)
    {
        PlayerPrefs.SetInt("CurrentLevel", targetValue);
    }

}
