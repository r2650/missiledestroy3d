using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#region Comment Out After SDK integrations
//using GameAnalyticsSDK;
//using Facebook.Unity; 
#endregion

public class SDKManager : MonoBehaviour
{

    public static SDKManager instance;

    private void Awake()
    {
        if (instance == null)
            instance = this;

        SetEvents(true);

        #region Comment Out after Facebook SDK integration
        //if (!FB.IsInitialized)
        //{
        //    // Initialize the Facebook SDK
        //    FB.Init(InitCallback, OnHideUnity);
        //}
        //else
        //{
        //    // Already initialized, signal an app activation App Event
        //    FB.ActivateApp();
        //} 
        #endregion

    }

    void TestFunction(string text)
    {
        Debug.Log(text);
    }

    private void OnDisable()
    {
        SetEvents(false);
    }

    void Start()
    {
        if(SceneSystem.instance.sceneType == SceneTypes.Logo)
        {
            //InitializeGameAnalyticsSDK();  -- Comment Out after integrated Game Analytics SDK to porject!
        }

        GingUtility.FunctionDelayer(() => TestFunction("Turan Abi"), 3);
    }

    #region Events
    private void SetEvents(bool enable)
    {
        if (enable)
        {
            EventManager.OnSceneStart += SceneStartSDKManager;
            EventManager.OnSceneEnd += SceneEndSDKManager;
        }
        else
        {
            EventManager.OnSceneStart -= SceneStartSDKManager;
            EventManager.OnSceneEnd -= SceneEndSDKManager;
        }
    }

    private void SceneStartSDKManager()
    {
        int currentLevel = PreferenceManager.instance.CurrentLevel();
        string value = string.Format("{0:D3}", currentLevel);

        //Comment Out after Game Anlalytics SDK integration
        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "Level_" + value);

        Debug.Log("Event Debug Level Start " + "Level_" + value);
    }

    private void SceneEndSDKManager()
    {
        if (GameManager.instance.didWin)
            WinSDKManager();
        else
            FailSDKManager();
    }

    private void WinSDKManager()
    {
        int currentLevel = PreferenceManager.instance.CurrentLevel();
        string value = string.Format("{0:D3}", currentLevel);

        //Comment Out after Game Anlalytics SDK integration
        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "Level_" + value);

        Debug.Log("Event Debug Level Win " + "Level_" + value);
    }

    private void FailSDKManager()
    {
        int currentLevel = PreferenceManager.instance.CurrentLevel();
        string value = string.Format("{0:D3}", currentLevel);

        //Comment Out after Game Anlalytics SDK integration
        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, "Level_" + value);

        Debug.Log("Event Debug Level Fail " + "Level_" + value);
    } 
    #endregion

    #region Facebook  -- Comment Out after Facebook SDK integration.
    //private void InitCallback()
    //{
    //    if (FB.IsInitialized)
    //    {
    //        // Signal an app activation App Event
    //        FB.ActivateApp();
    //        // Continue with Facebook SDK
    //        // ...
    //    }
    //    else
    //    {
    //        Debug.Log("Failed to Initialize the Facebook SDK");
    //    }
    //}

    //private void OnHideUnity(bool isGameShown)
    //{
    //    if (!isGameShown)
    //    {
    //        // Pause the game - we will need to hide
    //        Time.timeScale = 0;
    //    }
    //    else
    //    {
    //        // Resume the game - we're getting focus again
    //        Time.timeScale = 1;
    //    }
    //}
    #endregion

    #region GameAnalytics  -- Comment Out after Game Anlalytics SDK integration
    //private void InitializeGameAnalyticsSDK()
    //{
    //    GameAnalytics.SetBuildAllPlatforms("1.6");
    //    GameAnalytics.Initialize();
    //    Debug.Log("GA Initialized");

    //    if (Application.platform == RuntimePlatform.IPhonePlayer)
    //    {
    //        //GameAnalytics.RequestTrackingAuthorization(this);
    //    }
    //    else
    //    {
    //        GameAnalytics.Initialize();
    //    }

    //}

    //public void GameAnalyticsATTListenerNotDetermined()
    //{
    //    GameAnalytics.Initialize();
    //}
    //public void GameAnalyticsATTListenerRestricted()
    //{
    //    GameAnalytics.Initialize();
    //}
    //public void GameAnalyticsATTListenerDenied()
    //{
    //    GameAnalytics.Initialize();
    //}
    //public void GameAnalyticsATTListenerAuthorized()
    //{
    //    GameAnalytics.Initialize();
    //}
    #endregion

}
