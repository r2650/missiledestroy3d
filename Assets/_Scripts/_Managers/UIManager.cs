using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIManager : MonoBehaviour
{

    [SerializeField] GameObject gameCanvas;
    private GameObject logoCanvas, loadingCanvas, inGameCanvas, nonRestrictedInGameCanvas, levelEndBackGround, winGameBackground, loseGameBackfround, storeBackground, bannerBackground;
    private GameObject settingsTab, totalCoinParentObject, restrictedInGameCanvas, preGameParent, inGameParent, endGameParent, storeParent, tutorialParent, preGameTutorialParent;
    private GameObject inGameProgressPrent, healthBarParent, winGameParent, loseGameParent, unlockedItemBackground, unlockedItemParent;

    private Text totalCoinText, levelNumberText;
    private Button storeOpenButton, storeCloseButton, restartButton, skipLevelButton, settingsButton, startButtonFullScreen, startButtonSmall, nexLevelButton, loseLevelButton;
    [SerializeField] Image filledImage;
    [SerializeField] Text fillText;
    [SerializeField] Transform rightClouds;
    [SerializeField] Transform leftClouds;
    public static UIManager instance;
    private RocketMovement rocket;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        ReferenceSetter();
        //CanvasSetter();
        ButtonSetter();
        //SetCloudsForEnd();
    }
    public void OpenDestroyBar()
    {
        filledImage.transform.parent.parent.DOScale(1, .7f);
    }
    public void IncreaseDestroyBar(float val)
    {
        filledImage.fillAmount += val;
        int a = (int)(filledImage.fillAmount * 100);
        fillText.text = $"%{a}";
    }
    public void SetCloudsForEnd()
    {
        rightClouds.gameObject.SetActive(true);
        leftClouds.gameObject.SetActive(true);
        rightClouds.DOMoveX(-700, 1.5f).SetEase(Ease.Linear);
        leftClouds.DOMoveX(1500, 1.5f).SetEase(Ease.Linear);
    }

    public void TapToStart()
    {
        
        preGameParent.SetActive(false);
        rocket.StartMovement();
    }
    public void ShowEndGame()
    {
        if (filledImage.fillAmount > .5f)
        {
            winGameParent.transform.DOScale(1, 1).SetDelay(1).SetEase(Ease.InBounce);
        }
        else
            loseGameParent.transform.DOScale(1,1).SetDelay(1).SetEase(Ease.InBounce);
    }
    #region Standart Set Functions
    private void ReferenceSetter()
    {
        rocket = FindObjectOfType<RocketMovement>();
        logoCanvas = gameCanvas.transform.GetChild(0).gameObject;
        loadingCanvas = gameCanvas.transform.GetChild(1).gameObject;
        inGameCanvas = gameCanvas.transform.GetChild(2).gameObject;


        restrictedInGameCanvas = inGameCanvas.transform.GetChild(1).gameObject;

        preGameParent = restrictedInGameCanvas.transform.GetChild(0).gameObject;
        startButtonFullScreen = preGameParent.transform.GetChild(0).GetComponent<Button>();
        startButtonSmall = preGameParent.transform.GetChild(1).GetComponent<Button>();
        preGameTutorialParent = preGameParent.transform.GetChild(2).gameObject;

        inGameParent = restrictedInGameCanvas.transform.GetChild(1).gameObject;
        inGameProgressPrent = inGameParent.transform.GetChild(0).gameObject;
        healthBarParent = inGameParent.transform.GetChild(1).gameObject;

        endGameParent = restrictedInGameCanvas.transform.GetChild(2).gameObject;
        winGameParent = endGameParent.transform.GetChild(0).gameObject;
        nexLevelButton = winGameParent.transform.GetChild(0).GetComponent<Button>();
        loseGameParent = endGameParent.transform.GetChild(1).gameObject;
        loseLevelButton = loseGameParent.transform.GetChild(0).GetComponent<Button>();
        unlockedItemParent = endGameParent.transform.GetChild(2).GetChild(0).gameObject;

        levelNumberText = restrictedInGameCanvas.transform.GetChild(5).GetChild(0).GetChild(1).GetComponent<Text>();

        nonRestrictedInGameCanvas = inGameCanvas.transform.GetChild(0).gameObject;

        storeParent = restrictedInGameCanvas.transform.GetChild(3).gameObject;
        tutorialParent = restrictedInGameCanvas.transform.GetChild(4).gameObject;

        winGameBackground = nonRestrictedInGameCanvas.transform.GetChild(0).GetChild(0).gameObject;
        loseGameBackfround = nonRestrictedInGameCanvas.transform.GetChild(0).GetChild(1).gameObject;
        levelEndBackGround = nonRestrictedInGameCanvas.transform.GetChild(0).GetChild(2).gameObject;
        storeBackground = nonRestrictedInGameCanvas.transform.GetChild(0).GetChild(3).gameObject;
        unlockedItemBackground = nonRestrictedInGameCanvas.transform.GetChild(0).GetChild(4).gameObject;
        bannerBackground = nonRestrictedInGameCanvas.transform.GetChild(0).GetChild(5).gameObject;

        settingsTab = nonRestrictedInGameCanvas.transform.GetChild(1).GetChild(4).gameObject;
        totalCoinParentObject = nonRestrictedInGameCanvas.transform.GetChild(2).gameObject;

        totalCoinText = totalCoinParentObject.transform.GetChild(1).GetComponent<Text>();

        storeOpenButton = nonRestrictedInGameCanvas.transform.GetChild(1).GetChild(0).GetComponent<Button>();
        storeCloseButton = nonRestrictedInGameCanvas.transform.GetChild(1).GetChild(1).GetComponent<Button>();
        restartButton = nonRestrictedInGameCanvas.transform.GetChild(1).GetChild(2).GetComponent<Button>();
        skipLevelButton = nonRestrictedInGameCanvas.transform.GetChild(1).GetChild(3).GetComponent<Button>();
        settingsButton = settingsTab.transform.GetChild(1).GetComponent<Button>();

    }
    private void ButtonSetter()
    {
        nexLevelButton.onClick.RemoveAllListeners();
        nexLevelButton.onClick.AddListener(() =>
        {
            PreferenceManager.instance.SetCurrentLevel(PreferenceManager.instance.CurrentLevel() + 1);
            EventManager.SceneEndEvent();
        });

        restartButton.onClick.RemoveAllListeners();
        restartButton.onClick.AddListener(() =>
        {
            SceneSystem.instance.RestartGame();
        });

        loseLevelButton.onClick.RemoveAllListeners();
        loseLevelButton.onClick.AddListener(() =>
        {
            EventManager.SceneEndEvent();
        });
    }
    private void CanvasSetter()
    {
        switch (SceneSystem.instance.sceneType)
        {
            case SceneTypes.None:
                break;
            case SceneTypes.Logo:
                logoCanvas.SetActive(true);
                loadingCanvas.SetActive(false);
                inGameCanvas.SetActive(false);
                break;
            case SceneTypes.Loading:
                logoCanvas.SetActive(false);
                loadingCanvas.SetActive(true);
                inGameCanvas.SetActive(false);
                break;
            case SceneTypes.Game:
                logoCanvas.SetActive(false);
                loadingCanvas.SetActive(false);
                inGameCanvas.SetActive(true);
                break;
            default:
                break;
        }
    } 
    #endregion

}
