using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class RocketMultiplier : MonoBehaviour
{
    #region Variables
    [SerializeField] float minX, maxX;
    [SerializeField] List<GameObject> mainRocketsList;
    [SerializeField] GameObject rocketObject;
    [SerializeField] Transform parentObject;
    [SerializeField] List<GameObject> rockets1;
    [SerializeField] List<GameObject> rockets2;
    [SerializeField] List<GameObject> rockets3;
    [SerializeField] List<GameObject> rockets4;
    [SerializeField] List<GameObject> rockets5;
    [SerializeField] List<GameObject> rockets6;
    [SerializeField] Animator camAnim;
    public bool explodeOnFinish;
    public float distance;
    public int currentRocketCount = 1;
    private int lastRocketCount;
    private int rocketCounter = 0;
    private int ownRocketsCount = 1;
    public int totalPiece;
    public float bardivider;

    #endregion
    void Start()
    {
        lastRocketCount = 0;
        //InsertRocketsToTheLists();
        bardivider = 1.0f / totalPiece;

        //StartCoroutine(ChangeStartCam());
    }

    public IEnumerator ChangeStartCam()
    {
        yield return new WaitForSeconds(.1f);
        camAnim.SetTrigger("GameCam");
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            SortRockets();
        }
    }
    public void AddRocketToMainList(int rocketCount)
    {
        mainRocketsList.Clear();
        currentRocketCount += rocketCount;
        if (currentRocketCount > 275)
            currentRocketCount = 275;
        SetCameraDueToNumber();
        for (int i = 0; i < currentRocketCount; i++)
        {
            if (i > 0)
            {
                mainRocketsList.Add(parentObject.transform.GetChild(i).gameObject);
                parentObject.transform.GetChild(i).gameObject.SetActive(true);
            }

        }
        if(mainRocketsList.Count > 0)
            mainRocketsList[0].gameObject.SetActive(false);
        InsertRocketsToTheLists();
        lastRocketCount = currentRocketCount + 1;
    }
    public void RemoveRocketsFromMainList(int rocketCount)
    {
        currentRocketCount += rocketCount;
        if (currentRocketCount <= 0)
            currentRocketCount = 1;
        SetCameraDueToNumber();
        int count = mainRocketsList.Count;
        Debug.Log(count);
        if (count + rocketCount < 0)
            rocketCount = -count + 1;
        for (int i = count; i > count + rocketCount; i--)
        {
            parentObject.transform.GetChild(i).gameObject.SetActive(false);
            mainRocketsList.Remove(parentObject.transform.GetChild(i).gameObject);
        }
        RemoveRocketsFromTheLists();

        
        lastRocketCount = currentRocketCount + 1;
    }
    public void InsertRocketsToTheLists()
    {
        rockets1.Clear();
        rockets2.Clear();
        rockets3.Clear();
        rockets4.Clear();
        rockets5.Clear();
        rockets6.Clear();
        for (int i = 0; i < mainRocketsList.Count; i++)
        {
            if (i < 10)
                rockets1.Add(mainRocketsList[i]);
            else if (i >= 10 && i <= 30)
                rockets2.Add(mainRocketsList[i]);
            else if (i > 30 && i <= 60)
                rockets3.Add(mainRocketsList[i]);
            else if (i > 60 && i < 100)
                rockets4.Add(mainRocketsList[i]);
            else if (i > 100 && i < 160)
                rockets5.Add(mainRocketsList[i]);
            else if (i > 160 && i < 250)
                rockets6.Add(mainRocketsList[i]);
        }
        SortRockets();
    }
    private void RemoveRocketsFromTheLists()
    {
        int ommitedFromListCount = lastRocketCount - currentRocketCount - 1;
        for (int i = 0; i < ommitedFromListCount; i++)
        {
            if (rockets6.Count > 0)
                rockets6.Remove(rockets6[rockets6.Count - 1]);
            else if (rockets5.Count > 0)
                rockets5.Remove(rockets5[rockets5.Count - 1]);
            else if (rockets4.Count > 0)
                rockets4.Remove(rockets4[rockets4.Count - 1]);
            else if (rockets3.Count > 0)
                rockets3.Remove(rockets3[rockets3.Count - 1]);
            else if (rockets2.Count > 0)
                rockets2.Remove(rockets2[rockets2.Count - 1]);
            else if (rockets1.Count > 0)
                rockets1.Remove(rockets1[rockets1.Count - 1]);
        }
        //SortRockets();
        AddRocketToMainList(0);
    }

    private void SetCameraDueToNumber()
    {
        if (currentRocketCount > 70 && currentRocketCount < 120)
            camAnim.SetTrigger("GameCam1");
        else if (currentRocketCount > 120)
            camAnim.SetTrigger("GameCam2");
        else
            camAnim.SetTrigger("GameCam");
    }
    private void MoveRockets(Transform objectTransform, float degree)
    {
        Vector3 pos = Vector3.zero;
      //  pos.x = Random.Range(Mathf.Cos(degree * Mathf.Deg2Rad), Mathf.Cos(degree * 1.1f * Mathf.Deg2Rad));
       // pos.y = Random.Range(Mathf.Sin(degree * Mathf.Deg2Rad), Mathf.Sin(degree * 1.05f * Mathf.Deg2Rad));
        pos.x = Mathf.Cos(degree * Mathf.Deg2Rad);
        pos.y = Mathf.Sin(degree * Mathf.Deg2Rad);
        if (!explodeOnFinish)
            objectTransform.DOLocalMove(pos * (distance + Random.Range(0.3f,1.4f)), .5f);
        else
            objectTransform.DOLocalMove(pos * (distance + Random.Range(0.2f, 1.1f)), 4.8f);
    }
    public void SortRockets()
    {
        if (!explodeOnFinish)
        {
            for (int i = 0; i < mainRocketsList.Count; i++)
            {
                if (i == 1)
                    CalculateRockets(rockets1, 2);
                else if (i == 10)
                    CalculateRockets(rockets2, 4f);
                else if (i == 30)
                    CalculateRockets(rockets3, 6f);
                else if (i == 60)
                    CalculateRockets(rockets4, 8f);
                else if (i == 100)
                    CalculateRockets(rockets5, 10f);
                else if (i == 160)
                    CalculateRockets(rockets6, 12.0f);
            }
        }
        else
        {
            for (int i = 0; i < mainRocketsList.Count; i++)
            {
                if (i == 1)
                    CalculateRockets(rockets1, 40.0f);
                else if (i == 11)
                    CalculateRockets(rockets2, 55.0f);
                else if (i == 31)
                    CalculateRockets(rockets3, 70.0f);
                else if (i == 61)
                    CalculateRockets(rockets4, 85.0f);
                else if (i == 101)
                    CalculateRockets(rockets5, 100.0f);
                else if (i == 161)
                    CalculateRockets(rockets6, 120.0f);
            }
        }
        
    }

    private void CalculateRockets(List<GameObject> rocket, float distanceIncreaser)
    {
        float rocketCount = rocket.Count;
        float angle = 360 / rocketCount;

        for (int j = 0; j < rocketCount; j++)
        {
            MoveRockets(rocket[j].transform, j * angle);
            distance = distanceIncreaser;
        }
    }
    private void CalculateRockets2(int count, float distanceIncreaser)
    {
        float rocketCount = count;
        float angle = 360 / rocketCount;

        for (int j = 0; j < rocketCount; j++)
        {
            MoveRockets(mainRocketsList[j].transform, j * angle);
            distance = distanceIncreaser;
        }
    }

    public void IncreaseScaleOfRockets()
    {
        for (int i = 0; i < mainRocketsList.Count; i++)
        {
            mainRocketsList[i].transform.DOScale(3.0f, 3.7f);
        }
    }

    public void SetFinishCam()
    {
        StartCoroutine(DelayCam());

        IEnumerator DelayCam()
        {
            yield return new WaitForSeconds(.2f);
            camAnim.SetTrigger("FinishCam");
        }
    }
}
