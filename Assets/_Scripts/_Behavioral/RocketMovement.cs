using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Dreamteck.Splines;
public class RocketMovement : MonoBehaviour
{
    private UIManager UI;
    [SerializeField]
    private float horizontalMovementRestricter;
    [SerializeField]
    private float verticalMovementRestricter;
    [SerializeField]
    private float inputMultiplier;
    [SerializeField] Cinemachine.CinemachineVirtualCamera explosionCam;
    [SerializeField] Cinemachine.CinemachineVirtualCamera finishCam;
    private bool inputOpen;
    private Transform rotator;
    private Vector3 firstMousePos;
    private Vector3 currentMousePos;
    private float distanceBetweenVerticalPos;
    private float distanceBetweenHorizontalPos;
    private float yAxisPower;
    [SerializeField] GameObject explosionSprites;
    private float horizontalMovementVal;
    private float verticalMovementVal;
    [SerializeField] float speed;
    [SerializeField] GameObject environment;
    float x;
    float y;
    private Rigidbody rb;
    private float firstX;
    private float firstY;
    private RocketMultiplier multiplier;

    public SplineFollower follower;
    void Start()
    {
        //rb = GetComponent<Rigidbody>();
        rotator = transform.GetChild(0);
        distanceBetweenVerticalPos = 1.5f;
        follower = transform.parent.GetComponent<SplineFollower>();
        UI = FindObjectOfType<UIManager>();
        multiplier = FindObjectOfType<RocketMultiplier>();
    }

    void Update()
    {
        InputController();
    }

    private void InputController()
    {

        if (Input.GetMouseButton(0))
        {
            x = Input.GetAxis("Mouse X") * inputMultiplier;
            y = Input.GetAxis("Mouse Y") * inputMultiplier * 1.0f;
            transform.Translate(x, y, 0);

            if (transform.localPosition.x > horizontalMovementRestricter)
            {
                transform.localPosition = new Vector3(horizontalMovementRestricter, transform.localPosition.y, transform.localPosition.z);
            }
            else if (transform.localPosition.x < -horizontalMovementRestricter)
            {
                transform.localPosition = new Vector3(-horizontalMovementRestricter, transform.localPosition.y, transform.localPosition.z);
            }
            if (transform.localPosition.y > verticalMovementRestricter)
            {
                transform.localPosition = new Vector3(transform.localPosition.x, verticalMovementRestricter, transform.localPosition.z);
            }
            else if (transform.localPosition.y < -verticalMovementRestricter + 2)
            {
                transform.localPosition = new Vector3(transform.localPosition.x, -verticalMovementRestricter + 2, transform.localPosition.z);
            }

        }
       // rotator.rotation = Quaternion.Lerp(rotator.rotation, Quaternion.Euler(-y * 10, x * 10, 0), 4 * Time.deltaTime);
        //RotatorController();
    }

    private void RotatorController()
    {
        if (Input.GetMouseButtonDown(0))
        {
            firstMousePos = Input.mousePosition * .01f;
        }

        if (Input.GetMouseButton(0))
        {
            currentMousePos = Input.mousePosition * .01f;
            if(Vector3.Distance(firstMousePos,currentMousePos) > .1f)
            {
                distanceBetweenVerticalPos = firstY - (currentMousePos.y - firstMousePos.y);
                yAxisPower = currentMousePos.y - firstMousePos.y;
                x = -(firstMousePos.x - currentMousePos.x);
               // transform.localPosition = Vector3.Lerp(transform.localPosition, new Vector3(transform.localPosition.x + x, transform.localPosition.y + yAxisPower, transform.localPosition.z), 8 * Time.deltaTime);
            }                           
        }
       
        
        //Yukar� asagi karisiyor
        if (Input.GetMouseButtonUp(0))
        {
            //rotator.DORotate(Vector3.zero, .3f);
            //firstX = x;
            //x = firstX;
            firstY = distanceBetweenHorizontalPos;
            Debug.Log(firstX + " " + firstY);
        }
    }

    private void RigidBodyController()
    {
        //if (transform.position.x > horizontalMovementRestricter || transform.position.x < -horizontalMovementRestricter)
        //    x = 0;
        //else if (transform.position.y > verticalMovementRestricter || transform.position.y < -verticalMovementRestricter)
        //    distanceBetweenVerticalPos = 0;
        if (y > .1f || y < -.1f || x > .1f || x < -.1f)
        {
            rb.velocity = speed * transform.forward + Vector3.up * -distanceBetweenVerticalPos * 4.5f; //+ Vector3.right * x * 2f;
        }
       
        
    }

    public void StartMovement()
    {
        follower.follow = true;
        StartCoroutine(multiplier.ChangeStartCam());
    }

    public void ChangeExplosionCam()
    {
        FindObjectOfType<RocketMultiplier>().explodeOnFinish =true;
        horizontalMovementRestricter *= 20f;
        verticalMovementRestricter *= 10f;
        inputMultiplier += 2f;
        follower.followSpeed *= 1.5f; 
        StartCoroutine(CameraShakeOpen());
        IEnumerator CameraShakeOpen()
        {
            yield return new WaitForSeconds(.7f);
            FindObjectOfType<RocketMultiplier>().IncreaseScaleOfRockets();
            environment.SetActive(true);
            FindObjectOfType<Cinemachine.CinemachineStateDrivenCamera>().GetComponent<Animator>().SetTrigger("ExplosionCam");
            FindObjectOfType<RocketMultiplier>().SortRockets();
            yield return new WaitForSeconds(2.3f);
            explosionCam.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 1.5f;
            yield return new WaitForSeconds(2.4f);
            explosionCam.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 3.5f;
            explosionSprites.SetActive(true);
            yield return new WaitForSeconds(2.0f);
            explosionCam.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 1.2f;
            yield return new WaitForSeconds(1.0f);
            explosionCam.GetCinemachineComponent<Cinemachine.CinemachineBasicMultiChannelPerlin>().m_AmplitudeGain = 0.0f;
        }
    }

    public void CameraStopFollowing()
    {
        explosionCam.Follow = null;
        finishCam.Follow = null;
        StartCoroutine(OpenBar());
        IEnumerator OpenBar()
        {
            yield return new WaitForSeconds(.5f);
            UI.OpenDestroyBar();
        }
        
    }
}
