using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoomRockets : MonoBehaviour
{
    public float radius = 10.0F;
    public float power = 10.0F;
    private bool isHit;
    private List<Rigidbody> hittedObjects;
    [SerializeField] ParticleSystem explosionParticle;
    private UIManager UI;
    void Start()
    {
        UI = FindObjectOfType<UIManager>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!isHit)
        {
            if (collision.gameObject.CompareTag("NotShattered"))
            {
                if (collision.gameObject.GetComponent<MeshRenderer>())
                    collision.gameObject.GetComponent<MeshRenderer>().enabled = false;
                collision.gameObject.GetComponent<Collider>().enabled = false;
                collision.gameObject.layer = LayerMask.NameToLayer("Rocket");
                if (collision.transform.childCount < 1)
                {
                    Debug.Log(collision.gameObject);
                }
                collision.transform.GetChild(0).gameObject.SetActive(true);
                StartCoroutine(ParticleSpawner(collision.transform));
                for (int i = 0; i < collision.gameObject.transform.GetChild(0).childCount; i++)
                {
                    collision.gameObject.transform.GetChild(0).GetChild(i).GetComponent<Rigidbody>().isKinematic = false;
                }

            }
            Vector3 explosionPos = transform.position;
            Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
            foreach (Collider hit in colliders)
            {
                
                if (!hit.gameObject.CompareTag("Ground") && !hit.gameObject.CompareTag("Gate") && !hit.gameObject.CompareTag("NotShattered") && hit.gameObject.GetComponent<Rigidbody>())
                {
                    Rigidbody rb = hit.GetComponent<Rigidbody>();
                    hit.gameObject.layer = LayerMask.NameToLayer("Rocket");
                    hit.gameObject.tag = "Ground";
                    rb.isKinematic = false;
                    rb.AddExplosionForce(power, explosionPos + Vector3.up * .5f, radius, 3.0F);                    
                    UI.IncreaseDestroyBar(FindObjectOfType<RocketMultiplier>().bardivider);
                }
                if (hit.gameObject.CompareTag("Ground"))
                {
                    GetComponent<Rigidbody>().AddExplosionForce(power, transform.position, radius, 3.0F);
                }

                if (hit.gameObject.CompareTag("Prob"))
                {
                    if (hit.GetComponent<Rigidbody>())
                    {
                        Rigidbody rbb = hit.GetComponent<Rigidbody>();
                        rbb.isKinematic = false;
                        rbb.constraints = RigidbodyConstraints.None;
                        rbb.AddExplosionForce(power, explosionPos - Vector3.up, radius);
                    }
                    else
                    {
                        hit.gameObject.AddComponent<Rigidbody>();
                        hit.gameObject.GetComponent<Rigidbody>().AddExplosionForce(power, explosionPos - Vector3.up, radius);
                    }

                }
                
            }

            isHit = true;
            StartCoroutine(CloseObject());
        }
        
    }

    private IEnumerator CloseObject()
    {
        yield return new WaitForSeconds(.4f);

        GetComponent<MeshRenderer>().enabled = false;
        transform.GetChild(0).gameObject.SetActive(false);
        
    }
    private IEnumerator ParticleSpawner(Transform hit)
    {
        yield return new WaitForSeconds(Random.Range(0, .25f));
        var particle = Instantiate(explosionParticle, hit.position + Vector3.up * 2, Quaternion.identity);
        particle.transform.localScale = Vector3.one * Random.Range(35, 55);
    }
}
