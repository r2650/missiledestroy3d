using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
public class GateBehavior : MonoBehaviour
{
    [SerializeField] bool increaserGate;
    [SerializeField] int rocketAmount;
    [SerializeField] int rocketCount;
    [SerializeField] RocketMultiplier rocketMultiplier;
    [SerializeField] bool multiplierGate;
    [SerializeField] bool dviderGate;
    [SerializeField] bool moveX;

    private void Start()
    {
        ArrangeGate();
        if(moveX)
         MoveXStart();
    }
    private void MoveXStart()
    {
        transform.DOMove(transform.position - transform.right * 12, 2f).SetEase(Ease.Linear).OnComplete(() =>
        {
            MoveXEnd();
        });
    }
    private void MoveXEnd()
    {
        transform.DOMove(transform.position + transform.right * 12, 2f).SetEase(Ease.Linear).OnComplete(() =>
        {
            MoveXStart();
        });
    }
    private void ArrangeGate()
    {
        if(rocketAmount > 0)
        {
            Color tmp = transform.GetChild(0).GetComponent<SpriteRenderer>().color;
            tmp = Color.green;
            tmp.a = .85f;
            transform.GetChild(0).GetComponent<SpriteRenderer>().color = tmp;
            if(!multiplierGate && !dviderGate)
                transform.GetChild(0).GetChild(0).GetComponent<TextMeshPro>().text = $"+{rocketAmount}";
            else if(multiplierGate)
                transform.GetChild(0).GetChild(0).GetComponent<TextMeshPro>().text = $"X{rocketAmount}";
            else if (dviderGate)
            {
                transform.GetChild(0).GetChild(0).GetComponent<TextMeshPro>().text = $"/{rocketAmount}";
                tmp = Color.red;
                tmp.a = .7f;
                transform.GetChild(0).GetComponent<SpriteRenderer>().color = tmp;
            }
                
        }

        else
        {
            Color tmp = transform.GetChild(0).GetComponent<SpriteRenderer>().color;
            tmp = Color.red;
            tmp.a = .7f;
            transform.GetChild(0).GetComponent<SpriteRenderer>().color = tmp;
            transform.GetChild(0).GetChild(0).GetComponent<TextMeshPro>().text = $"{rocketAmount}";
        }
            

        
    }
    private void OnTriggerEnter(Collider other)
    {
        GetComponent<Collider>().enabled = false;
        if (!multiplierGate && !dviderGate)
            rocketCount += rocketAmount;
        else if(multiplierGate && !dviderGate)
        {
            //if (rocketCount == 0)
                rocketCount = rocketMultiplier.currentRocketCount;
            rocketCount *= rocketAmount;

        }
        else if(dviderGate && !multiplierGate)
        {
            rocketCount = rocketMultiplier.currentRocketCount;
            rocketCount -= (rocketCount / rocketAmount);
            rocketCount = -rocketCount;
        }
            
        if (increaserGate)
            rocketMultiplier.AddRocketToMainList(rocketCount);
        else
            rocketMultiplier.RemoveRocketsFromMainList(rocketCount);
        gameObject.SetActive(false);
    }
}
