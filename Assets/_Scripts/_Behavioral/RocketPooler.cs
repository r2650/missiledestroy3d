using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketPooler : MonoBehaviour
{
    [System.Serializable]
    public class Pool
    {
        public string tag;
        public GameObject prefab;
        public int size;
        public Transform parentObject;
    }
    public List<Pool> pools;
    public Dictionary<string, Queue<GameObject>> rocketDictionary;
    void Start()
    {
        rocketDictionary = new Dictionary<string, Queue<GameObject>>();

        foreach (Pool pool in pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();

            for (int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.prefab, pool.parentObject.transform.position, Quaternion.Euler(Vector3.zero), pool.parentObject);
                obj.SetActive(false);
                obj.transform.localRotation = Quaternion.Euler(100,0,0);
                objectPool.Enqueue(obj);
            }

            rocketDictionary.Add(pool.tag, objectPool);
        }
    }

}
