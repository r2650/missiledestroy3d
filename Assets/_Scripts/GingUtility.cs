using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GingUtility 
{

    public static void FunctionDelayer(System.Action functionToDelay, float delayDuration)
    {
        if (GameManager.instance!= null)
        {
            GameManager.instance.StartCoroutine(WaitCoroutine());
            IEnumerator WaitCoroutine()
            {
                yield return new WaitForSeconds(delayDuration);
                functionToDelay();
            }
        }
    }

}